package bubble_algorithm;

import java.util.Random;

public class BubbleAlgorithm {
	
	int[] array = new int[20]; // combining both statements in one
		
	public int[] sort_array(String order, int[] array){
		
		this.array = array;
		boolean sort = false;
		int aux;
		
		do{
			sort = true;
			for (int i = 0; i < array.length;)
			{
				if (order.equals("UP"))
					if ((i<array.length-1) && (array[i] > array[i+1]))
					{
						aux = array[i];
						array[i] = array[i+1];
						array[i+1] = aux;
						sort = false;
				}
				if (order.equals("DOWN"))
					if ((i<array.length-1) && (array[i] < array[i+1]))
					{
						aux = array[i];
						array[i] = array[i+1];
						array[i+1] = aux;
						sort = false;
				}
				
				i++;
			}
		}while (!sort);		
		
		return array;
	}
	
		
	public int[] get_array_numbers(int max, int min){
		
		int point;
		Random seed = new Random();
		
		for (int i = 0; i < array.length; i++)
		{
			point = seed.ints(min, max).findFirst().getAsInt();
			array[i] = point;
		}
		return array;
	}	
}
