package bubble_algorithm;
import java.util.*;


public class main {
	
	public static void main(String[] args) { 
		int max=0;
		int min=0;
		String order = null;
		int [] sortArray;
		try {
		for(int i=0;i<args.length;i++) { 
			System.out.println("Arg_" + i + ": " + args[i]);
			// TODO check args !!!!!!!!!!!!!!!!!!!!!!!
			max = Integer.parseInt(args[0]);
			min = Integer.parseInt(args[1]);
			order = args[2];
			}  
		}catch(Exception e) {
			System.out.println(e.getCause());
		}
		
		BubbleAlgorithm bubbleGum = new BubbleAlgorithm();
		sortArray = bubbleGum.get_array_numbers(max, min);
		sortArray = bubbleGum.sort_array(order, sortArray);		
		System.out.println("\nSorted array by bubble_algorithm class: " + Arrays.toString(sortArray));
			
	}
}


